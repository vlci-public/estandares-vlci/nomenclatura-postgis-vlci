**Índice**  

 [[_TOC_]]  
  
  
# Nomenclatura de los componentes de una base de datos


## Nombre de las tablas de base de datos

En nuestras bases de datos podemos diferenciar las tablas en 3 tipos:
- Tablas de referencia: los datos de estas tablas los gestionan los programadores pero los datos no varían entre entornos
- Tablas de entorno: los datos de estas tablas los gestionan los programadores y que además cambian en función del entorno donde se encuentran
- Tablas de datos: los datos de estas tablas son dinámicos y los producen las ETLs u otros procesos (Ej: Cygnus)


### Tablas de referencia
t_ref_[AREA_FUNCIONAL]_[DESCRIPCION]

**AREA_FUNCIONAL**
Una única palabra. Si aplica a muchas cosas: horizontal, si no, el nombre del área funcional.
Ejemplos de AREAS_FUNCIONALES:
- etloader
- bdo
- emt
- horizontal

**DESCRIPCION**
Varias palabras; describe el contenido de la tabla.

Ejemplos de nombres de tablas:
- t_ref_cdmu_indicadores
- t_ref_horizontal_gestion_fecha


### Tablas de entorno
t_env_[AREA_FUNCIONAL]_[DESCRIPCION]

Ejemplos de nombres de tablas:
- t_env_etloader_parametros_carga
- t_env_etloader_conexiones_ftp


### Tablas de datos
t_datos_[ORIGEN]\_[AREA_FUNCIONAL]\_[DESCRIPCION]

**ORIGEN**
Valores posibles: cb, etl, sql

Ejemplos de nombres de tablas:
- t_datos_sql_agregados_conversion_datos
- t_datos_cb_emt_kpi


## Nombre de las vistas de base de datos
Se utilizará la misma nomenclatura que para las tablas cambiando la 't' inicial por 'vw'

- vw_datos_[ORIGEN]\_[AREA_FUNCIONAL]\_[DESCRIPCION]
- vw_env_[AREA_FUNCIONAL]_[DESCRIPCION]
- vw_ref_[AREA_FUNCIONAL]_[DESCRIPCION]


# Buenas prácticas

## Nomenclatura

- **Lowercase. Identifiers should be written entirely in lower case**. This includes tables, views, column, and everything else too. Mixed case identifier names means that every usage of the identifier will need to be quoted in double quotes (which we already said are not allowed). Ex: Use first_name, not ""First_Name"".
- **Data types are not names**. Database object names, particularly column names, should be a noun describing the field or object. Avoid using words that are just data types such as text or timestamp. The latter is particularly bad as it provides zero context.
- **Underscores separate words (snake case)**. Object name that are comprised of multiple words should be separated by underscores. Ex: Use word_count or team_member_id, not wordcount or wordCount.
- **Full words, not abbreviations**. Object names should be full words. In general avoid abbreviations, especially if they're just the type that removes vowels. Most SQL databases support at least 30-character names which should be more than enough for a couple English words. PostgreSQL supports up to 63-character for identifiers. Ex: Use middle_name, not mid_nm.

## Campos de auditoría
- Todas las tablas tienen 4 campos de auditoria (Excepto tablas procedentes de CB): aud_fec_ins, aud_user_ins, aud_fec_upd, aud_user_upd
- Todas las tablas tienen los triggers de auditoria (Excepto tablas procedentes de CB). Ya existen funciones que implementan esos triggers. Usar las funciones existentes en general.

## Sentencias SQL
- Nunca usar el select *
- Si contienen sentencias de insert, rellenan los campos aud_user_ins y aud_user_upd con USUARIO-JIRA
- Si contienen sentencias de update, rellenan el campo aud_user_upd con USUARIO-JIRA

## Funciones/Procedimientos
- Si contienen sentencias de insert, rellenan los campos aud_user_ins y aud_user_upd con Nombre ETL
- Si contienen sentencias de update, rellenan el campo aud_user_upd con Nombre ETL

## GIT
- Las feature branches (FB) se crean desde la rama de test.
- El MR se realiza desde una rama de Feature a test.
- El MR debe marcar el check de borrar rama (Si no lo esta, se puede permitir con causa justificada)

## SQITCH
- Siempre crear Revert y Verify
- Ubicación de los scripts acorde al tipo de codigo: [ShP]\10. HowTo\HowTo - Postgres - Sqitch - Desarrollo en BBDD.docx
